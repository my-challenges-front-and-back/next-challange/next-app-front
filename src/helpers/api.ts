export const apiGetClients = async (params: string) => {
  const testando = await fetch(`http://localhost:5050/api/${params}`);
  return await testando.json()
}

export const addClient = async (data: any) => {
  const add = await fetch(`http://localhost:5050/api/add`, {
    method: 'POST',
    body: data,
  })
  return await add.json()
}

export const apiDeleteClient = async (id: string) => {
  const request = await fetch(`http://localhost:5050/api/del/${id}`, {
    method: 'DELETE',
    body: id,
  })
  return await request.json()
}

export const apiGetSingleClient = async (id: string) => {
  const request = await fetch(`http://localhost:5050/api/list/${id}`);
  return await request.json()
}

export const apiUpdateClient = async (id: string, data: any) => {
  const request = await fetch(`http://localhost:5050/api/edit/${id}`, {
    method: 'PUT',
    body: data
  });
  return await request.json()
}
// export default apiGet