import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/Home'
import AddClient from './pages/AddClient/AddClient'
import EditClient from './pages/EditClient/EditClient'

const Routers = () => (
    <BrowserRouter>
        <Routes>
           <Route path="/"  element={<Home />} />
           <Route path="/add" element={<AddClient />} />
           <Route path="/edit/:id" element={<EditClient/> } />
        </Routes>
    </BrowserRouter>
);

export default Routers;
