import * as yup from 'yup'

const clientSchemma = yup.object().shape({
  name: yup.string().required().min(1),
  cpf: yup.string().required().min(11),
  birthday_date: yup.string().required(),
  sale: yup.number().required(),
  limit: yup.number().required(),
  zip_code: yup.string().required().min(8),
  logradouro: yup.string().required(),
  district: yup.string().required(),
  number: yup.string().required(),
  city: yup.string().required(),
  uf: yup.string().required(),
})

export default clientSchemma