import styled from 'styled-components'



export const ProfileAdd = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  position: relative;
  border: 9px solid rgb(240, 241, 247);
`

export const Add = styled.label`
  width: 40px;
  height: 40px;
  background: #D4EDDA;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border-radius: 50%;
  position: absolute;
  bottom: -10px;
  right: -20px;
`


export const Remove = styled.label`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  bottom: -10px;
  left: -20px;
  background: #F8D7DA;
`

export const Profile = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 50%;
  object-fit: cover;
`