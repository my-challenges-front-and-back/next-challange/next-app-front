import React, { useState } from 'react'
import * as S from './styled'
import InputMask from 'react-input-mask';
import { Container, Form, Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import NumberFormat from 'react-number-format'
import clientScheme from '../../validations/clientValidation'
import { addClient } from '../../helpers/api';
import { Navigate } from 'react-router-dom';



function AddClient() {

  const [cep, setCep] = useState('')
  const [name, setName] = useState('')
  const [cpf, setCpf] = useState('')
  const [city, setCity] = useState('')
  const [date, setDate] = useState('')
  const [limit, setLimit] = useState('')
  const [uf, setUf] = useState('')
  const [sale, setSale] = useState('')
  const [number, setNumber] = useState('')
  const [logradouro, setLogradouro] = useState('')
  const [district, setDistrict] = useState('')
  const [avatar, setAvatar] = useState('')
  const [imagePreview, setImagePreview] = useState('')
  const formData = new FormData();
  const [error, setError] = useState()
  const [showError, setShowError] = useState(false)
  const navigate = useNavigate()

  const addProfile = async (profile: any) => {
    setAvatar(profile.target.files[0])
    setImagePreview(window.URL.createObjectURL(profile.target.files[0]))
    formData.append('avatar', profile.target.files[0]);
  }

  const searchCep = async () => {
    const cleanCep = cleanMask(cep)
    const cepRequest = await fetch(`https://viacep.com.br/ws/${cleanCep}/json/`)
    const cepResponse = await cepRequest.json()
    if(cepResponse) {
      setCity(cepResponse.localidade)
      setUf(cepResponse.uf)
      setLogradouro(cepResponse.logradouro)
      setDistrict(cepResponse.bairro)
    }
  }

  const cleanMask = (maskValue: string) => {
    const cleanMask = maskValue.replace(/\D/g, '')
    return cleanMask
  }

  const handleForm = async (event: any) => {
    event.preventDefault()
    formData.append('cpf', cleanMask(cpf))
    formData.append('name', name)
    formData.append('city', city)
    formData.append('birthday_date', date)
    formData.append('limit', cleanMask(limit))
    formData.append('sale', cleanMask(sale))
    formData.append('uf', uf)
    formData.append('number', number)
    formData.append('logradouro', logradouro)
    formData.append('district', district)
    formData.append('zip_code', cleanMask(cep))
    formData.append('avatar', avatar)
   

    const forms = {
      name, 
      cpf: cpf,
      birthday_date: date,
      city,
      limit: cleanMask(limit),
      sale: cleanMask(sale),
      uf,
      number,
      logradouro,
      district,
      zip_code: cleanMask(cep), 
    }

    const clientValid = await clientScheme.isValid(forms)
    if(clientValid) {
      const add = await addClient(formData)
      
      if(add.error) {
        setError(add.error)
        setShowError(true)
        setTimeout(() => setShowError(false), 4000)
      } else {
        navigate('/')
      }
    }

  }


  return (
    <Container className="mt-4 w-30">
      <h2>Cadastro de cliente</h2>
      <br />
      <br />
      {showError && 
        <div className="alert alert-danger text-center w-50 m-auto mb-4" role="alert">
        {error}!
        </div>
      }
      
      <Form className="w-50 m-auto mb-5" method="POST" onSubmit={handleForm}>
        <fieldset className="mb-4 d-flex justify-content-center">
           <Form.Group>
             <S.ProfileAdd className="d-flex justify-content-center align-items-center" >
             
               {imagePreview ? <S.Profile src={imagePreview} />: <span className="text-secondary">Avatar</span>}
               
               <S.Add htmlFor="editProfile">
                  <svg xmlns="http://www.w3.org/2000/svg"   viewBox="0 0 24 24" fill="none" color="#fff" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" width="25" className="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                  <input type="file" id="editProfile" className="d-none" accept="image/png, image/gif, image/jpeg" onChange={addProfile} required/>
               </S.Add>
               <S.Remove onClick={() => setImagePreview('')}>
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" fill="#fff"
                    width="25px" height="25px" viewBox="0 0 92 92" enableBackground="new 0 0 92 92">
                  <path id="XMLID_38_" d="M68,50.5H24c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5h44c2.5,0,4.5,2,4.5,4.5S70.5,50.5,68,50.5z"/>
                  </svg>
               </S.Remove>
             </S.ProfileAdd>
           </Form.Group>
        </fieldset>
        <fieldset>
          <Form.Group className="mb-3">
            <Form.Label htmlFor="disabledTextInput">Nome</Form.Label>
            <Form.Control id="disabledTextInput" placeholder="Digite aqui..." required value={name} onChange={name => setName(name.target.value)} />
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-between">
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">CPF</Form.Label>
              <InputMask mask="999.999.999-99" className="form-control" value={cpf} onChange={cpf => setCpf(cpf.target.value)}></InputMask>
            </div>
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Nascimento</Form.Label>
              <Form.Control id="disabledTextInput"  type="date" className="w-45" placeholder="00/00/0000" value={date} onChange={date => setDate(date.target.value)} required/>
            </div>
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-between">
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Saldo</Form.Label>
              <NumberFormat thousandSeparator={true} prefix={'R$ '} className="form-control" required placeholder="R$ 0,00" value={sale} onChange={(v: any) => setSale(v.target.value)} />
            </div>
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Limite</Form.Label>
              <NumberFormat thousandSeparator={true} prefix={'R$ '} className="form-control" required placeholder="R$ 0,00" value={limit} onChange={(l: any) => setLimit(l.target.value)} />
            </div>
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-between align-items-end">
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Cep</Form.Label>
              <InputMask mask="99999-999" className="form-control" value={cep} onChange={cep => setCep(cep.target.value)} pattern="[0-9]{5}-?[\d]{3}"></InputMask>
            </div>
            <div style={{ width: '45%' }}>
              <Button className="w-100" disabled={/[0-9]{5}-?[\d]{3}/.test(cep) ? false : true} onClick={searchCep}>Completar</Button>
            </div>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label htmlFor="disabledSelect">Logradouro</Form.Label>
            <Form.Control id="disabledTextInput" placeholder="Endereço" required value={logradouro}  onChange={log => setLogradouro(log.target.value)} />
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-between">
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Bairro</Form.Label>
              <Form.Control id="disabledTextInput" placeholder="Endereço" required value={district} onChange={dis => setDistrict(dis.target.value)} />
            </div> 
            <div  style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Número</Form.Label>
              <Form.Control id="disabledTextInput" placeholder="N°" required value={number} onChange={number => setNumber(number.target.value)} />
            </div> 
          </Form.Group>
          <Form.Group className="mb-3 d-flex justify-content-between">
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">Cidade</Form.Label>
              <Form.Control id="disabledTextInput" placeholder="Cidade" required value={city} onChange={cit => setCity(cit.target.value)} />
            </div> 
            <div style={{ width: '45%' }}>
              <Form.Label htmlFor="disabledSelect">UF</Form.Label>
              <Form.Control id="disabledTextInput" placeholder="Uf" required value={uf} onChange={uf => setUf(uf.target.value)} />
            </div> 
          </Form.Group>
          <Button type="submit" className="w-100 mt-4">Salvar</Button>
        </fieldset>  
    </Form>
    </Container>
  )
}

export default AddClient