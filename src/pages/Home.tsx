import React, { useEffect, useState } from 'react'
import {  useNavigate } from 'react-router-dom'
import  'bootstrap/dist/css/bootstrap.min.css'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import Pagination from 'react-bootstrap/Pagination'
import { apiDeleteClient, apiGetClients } from '../helpers/api';


function Home () {
  
  const [clients, setClients] = useState<any>([])
  const [pageList, setPageList] = useState(0)
  const [page, setPage] = useState(0)
  const [updatedScreen, setUpdatedScreen] = useState(false)
  const navigate = useNavigate()
  
  const searchClients = async () => {
    const clients = await apiGetClients(`/list?page=${page === 1 ? 0 : page}&offset=7`)
    setClients(clients)
    setPageList(Math.ceil(clients[1].total / 7))
  }

  const handlePage = (page: number) => {
    setPage(page)
  }

  const range = (page: number) => {
    let countPage = [];
    for(let i = 1; i <= page; i++) {
      countPage.push(i)
    }
    return countPage;
  }

  const deleteClient = async (id: string) => {
    const toDelete = window.confirm('Deseja deletar')
    if(toDelete) {
      await apiDeleteClient(id)
      setUpdatedScreen(true)
    } 
  }

  useEffect(() => {
    searchClients()
  }, [page, updatedScreen])
 
  return (
    <div className="App">
      <Container className="mt-5">
        <Button className="mb-4" onClick={() => navigate('/add')}>Adicionar Cliente</Button>
      <Table striped bordered>
        <thead>
          <tr>
            <th>#</th>
            <th>NOME</th>
            <th>CPF</th>
            <th className="col-md-3">LIMITE</th>
          </tr>
        </thead>
        <tbody>
          {clients[0] && 
            clients[0].map( (item: any, key: number) => (
               <tr key={key}>
                <td>{item._id.slice(2, 7)}</td>
                <td>
                  <img src={`http://localhost:5050/profile/${item.avatar}`} className="avatar" alt=""/>
                  {item.name}
                </td>
                <td>{item.cpf}</td>
                <td className="d-flex align-items-center justify-content-between">
                  <span>R$ {item.limit}</span>
                  <div>
                    <Button variant="warning" style={{marginRight: 10}} onClick={() => navigate(`/edit/${item._id}`)}>Editar</Button>
                    <Button variant="danger" onClick={() => deleteClient(item._id)}>Excluir</Button>
                  </div>
                </td>
               </tr>
            ) )
          }
        </tbody>
      </Table>
      <Pagination className="d-flex justify-content-center">
          <Pagination.Prev onClick={() => handlePage(page - 1)} disabled={page === 1 ? true : false} />
          {range(pageList).map( (item) => (
            <>
              <Pagination.Item active={(page === item || item === 0)? true : false} onClick={() => handlePage(item)}>{item}</Pagination.Item>
            </>
          ) )}
          <Pagination.Next onClick={() => handlePage(page + 1)} disabled={page === pageList ? true : false} />
      </Pagination>
      </Container>
     </div>
  );

}

export default Home